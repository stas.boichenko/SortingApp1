package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class MainTest {

    private String[] testArray;
    private Main sortingApp = new Main();

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"dgdfgdfg"},
                {"1034dgd9df305"},
                {"eretertre6667"},
                {"JHDI4564mrgdgkk"},
                {"1034305"},
                {"103430fxvdf5"},
                {"sfjbdfgd76"},
                {"3454398gg"},
                {"dfdg8888"},
                {"666666"},
                {"fffff"},
                {"vmnnbmmnnnm kk8"},
                {"dfgdkdfg88"},
                {"bfghfghfgh"},
        });
    }

    public MainTest(String testString) {
        this.testArray = testString.split("");
    }

    @Test
    public void testSortStream(){

        String expected = Arrays.stream(testArray).sorted().collect(Collectors.joining());

        assertEquals(expected, sortingApp.createSortedStream(testArray).collect(Collectors.joining()));
    }

    @Test
    public void testZero(){
        assertEquals("", sortingApp.createSortedStream(new String[0]).collect(Collectors.joining()));
    }

    @Test
    public void testOne(){
        assertEquals("5", sortingApp.createSortedStream(new String[]{"5"}).collect(Collectors.joining()));
    }

}