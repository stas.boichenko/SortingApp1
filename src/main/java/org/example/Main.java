package org.example;

import java.util.Arrays;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        Main sortingApp = new Main();
        sortingApp.createSortedStream(args)
                .forEach(System.out::println);
    }

    /**
     * Create sorted String stream
     * @param args arrays of args from command line
     * @return sorted sream
     */
    public Stream<String> createSortedStream(String[] args){
        return Arrays.stream(args)
                .sorted();
    }
}